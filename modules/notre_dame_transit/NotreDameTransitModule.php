<?php


class NotreDameTransitModule extends KGOModule {

    /*
     *  The initializeForPageConfigObjects_ methods below don't need to do much, they simply check if a feed has been configured
     *  The $objects configured in the page objdefs will take control from here
     */

    private $submittedData;
    private $shortNames;
    private $descriptions = NULL;

    protected function initializeForPageConfigObjects_index(KGOUIPage $page, $objects) {
        if (!($feed = $this->getFeed())) {
            return;
        }
    }

    protected function initializeForPageConfigObjects_submitted(KGOUIPage $page, $objects) {
    }

    protected function initializeForPage_osnap(KGOUIPage $page) {
        if (!($feed = $this->getFeed())) {
            return;
        }
        #check if open or closed
        $feed = $this->getFeed();
        $retr = $feed->getRetriever();
        $data = $retr->getData();
        $statusData = $data[0];
        $status = $statusData["serviceStatus"];
        if ($status == 1) {
            #load osnap page
            $page->appendToRegionContents('content', $this->getForm('osnap'));
        }   
        else {
            #load closed
            $page->appendToRegionContents('content', $this->getForm('closed'));
            // kgo_debug(array(KGODataController::factory($objdef, $this)), true, true);
            // kgo_debug($page, true, true);
        }        
    }

    protected function initializeForPageConfigObjects_closed(KGOUIPage $page, $objects) {
    }

    protected function initializeForPage_transpo(KGOUIPage $page) {

        //check what day
        //0 is sunday, 6 is saturday
        switch (date('w')) {
            case 0: //sunday
                $objdef = $this->getConfig('page-transpo_sunday.objdefs.content.default');
                break;
            case 5: //friday
                $objdef = $this->getConfig('page-transpo_friday.objdefs.content.default');
                break;
            case 6: //saturday
                $objdef = $this->getConfig('page-transpo_saturday.objdefs.content.default');
                break;
            default: //weekday
                $objdef = $this->getConfig('page-transpo_weekday.objdefs.content.default');
                break;
        }

        $this->addObjectDefinitionsToPage($page, $objdef, $this, $this, true);
    }

    protected function initializeForPage_upmall_station_weekday(KGOUIPage $page, $objects) {
        $table = new KGOUITable();
        $config = $this->getConfig('page-upmall_station_weekday-config');
        $controller = KGOTableController::factory($config);
        $table->setUIInterfaceDataSource($controller);

        $attributeMap = $this->getConfig('page-upmall_station_weekday-data.map');
        $data = $this->getConfig('page-upmall_station_weekday-data.data');
        $items = $this->selectTimes($data, $attributeMap);

        $controller->setItems($items);
        $page->appendToRegionContents('content', $table);
    }

    protected function initializeForm_osnap() {
        $objdef = $this->getConfig('page-osnap');
        $controller = KGOFormController::factory($objdef, $this);
        return $controller;
    }

    protected function initializeForm_closed() {
        $objdef = $this->getConfig('page-closed');
        $controller = KGOFormController::factory($objdef, $this);
        return $controller;
    }


    protected function processForm_osnap($form) {
        if($form->isValid()) {
            $submittedValues = $form->getSubmittedValues();

            // $url = 'http://requestb.in/1a0cra71';
            $url = "https://osnap.nd.edu/api/rides/";
            $data = $submittedValues;
            $this->submittedData = $data;

            // use key 'http' even if you send the request to https://...
            $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data),
                ),
            );
            $context  = stream_context_create($options);
            $response = file_get_contents($url, false, $context);
            $result = json_decode($response, true);
            $this->submittedData["id"] = $result["_id"];
        }
    }

    public function getLocations() {
        if($feed = $this->getFeed()) {
            $data = $feed->getRetriever()->getData();
            $thisData = $data[0];
            $locationsArray = $thisData['locations'];
            //$locationsArray = $feed->getRetriever()->getData()[0]['locations'];
            $locationsNamesArray = array();
            $this->shortNames = array();
            foreach ($locationsArray as $locationItem) {
                if($locationItem["disabled"] == false) {
                    $locationsNamesArray[$locationItem["name"]] = $locationItem["name"];
                    $this->descriptions[$locationItem["name"]] = $locationItem["description"];
                }
            }
            return $locationsNamesArray;
        } 
    }

    public function getNumberOfPassengers() {
        if($feed = $this->getFeed()) {
            $data = $feed->getRetriever()->getData();
            $adminData = $data[0];
            $ridersArray = array();
            for ($i=1; $i <= $adminData["maxRiders"]; $i++) { 
                 $ridersArray[$i] = strval($i);
             }
            return $ridersArray;
        }   
    }

    public function getShortNamesDict() {
        if($this->shortNames != NULL)
            return $this->shortNames;
        else
            $this->loadSubmittedData();

        return $this->shortNames; 
    }

    public function getDescriptionsDict() {
        if($this->descriptions != NULL)
            return $this->descriptions;
        else
            $this->loadSubmittedData();

        return $this->descriptions; 
    }

    private function loadSubmittedData() {
        if($feed = $this->getFeed()) {
            $data = $feed->getRetriever()->getData();
            $thisData = $data[0];
            $locationsArray = $thisData['locations'];
            $this->shortNames = array();
            $this->descriptions = array();
            foreach ($locationsArray as $locationItem) {
                $this->shortNames[$locationItem["name"]] = $locationItem["shortName"];
                $this->descriptions[$locationItem["name"]] = $locationItem["description"];
            }
        } 
    }

    public function getMapImageURL() {
        $base = "https://tlt-kgo.s3.amazonaws.com/kurogo/transit/osnap/images/";
        $shortNames = $this->getShortNamesDict();
        $location = $this->submittedData["location"];
        $shortName = $shortNames[$location];
        return KGOURL::createForString($base.$shortName."-map.jpg");
    }

    public function getLocationImageURL() {
        $base = "https://tlt-kgo.s3.amazonaws.com/kurogo/transit/osnap/images/";
        $shortNames = $this->getShortNamesDict();
        $location = $this->submittedData["location"];
        $shortName = $shortNames[$location];
        return KGOURL::createForString($base.$shortName."-pic.jpg");
    }

    public function getLocationDescription() {
        $descriptionsDict = $this->getDescriptionsDict();
        $loc = $this->submittedData["location"];
        $description = $descriptionsDict[$loc];
        return "Your pickup will be at: " . $this->submittedData["location"] . ", ". $description;
    }

    public function getActiveRidesStatus() {
        $feeds = $this->getAllFeeds();
        $rideFeed = $feeds["osnap_rides"];
        $retr = $rideFeed->getRetriever();
        $data = $retr->getData();
        // $rides = $this->getAllFeeds()["osnap_rides"]->getRetriever()->getData()["active"];
        $rides = $data["active"];
        #change to 0 if null
        if (!$rides) {
            $rides = "0";
        }
        $status = "There are ". $rides ." people ahead of you.";
        if($rides >= 5) #number until popular
            $status = "&#9888; O'SNAP is very popular right now. " . $status;

        return $status;

    }

    public function getRideLink() {

        return KGOUrl::createForModuleWebPage("transit", "osnap");

        // $feed = $this->getFeed();
        // $retr = $feed->getRetriever();
        // $data = $retr->getData();
        // $statusData = $data[0];
        // $status = $statusData["serviceStatus"];
        // if ($status == 1) {
        //     return KGOUrl::createForModuleWebPage("transit", "osnap");
        // }
        // else {
        //     return KGOUrl::createForModuleWebPage("transit", "closed");
        // }
        // if(date('D') == "Thu" || date('D') == "Fri" || date('D') == "Sat") {
        //     if(date('H') > 21 || date('H') < 3)
        //         return KGOUrl::createForModuleWebPage("transit", "osnap");
        //     else
        //         return KGOUrl::createForModuleWebPage("transit", "closed");
        // }
        // else {
        //     if (date('H') >= 22 || date('H') <= 2)
        //         return KGOUrl::createForModuleWebPage("transit", "osnap");
        //     else if((date('H') == 21 && date('i') >= 30) || (date('H') == 2 && date('i') <= 30))
        //         return KGOUrl::createForModuleWebPage("transit", "osnap");
        //     else
        //         return KGOUrl::createForModuleWebPage("transit", "closed");
        // }
    }

    protected function initializeForPageConfigObjects_shuttles(KGOUIPage $page, $objects) {
    }

    protected function selectTimes($times, $attributeMap) {
        $items = array();
        foreach ($times as $itemData) {
            $time = $itemData["time"];

            // This new parsing does not rely on the string being only time
            // This fixes the bug with time titles with other words
            $timeBreak = strpos($time, ":");
            if ($timeBreak == false) {
                $items[] = KGODataObject::parseItem($itemData, $attributeMap);
            }
            else {
                $components = explode(":", $time);

                //Change 12 to 0 to make all of the math much easier :)
                if ($components[0] == "12") {
                    $components[0] = "0";
                }

                $currHour = date('g');
                $currHour = $currHour == "12" ? "0" : $currHour;
                $currMin = date('i');

                if(substr($time, $timeBreak + 3, 2) == "PM" && date('A') == "AM") {
                    $items[] = KGODataObject::parseItem($itemData, $attributeMap);
                    continue;
                }
                //kgo_debug(substr($time, $timeBreak + 3, 2), true, true);
                if (substr($time, $timeBreak+3, 2) == date('A')) {
                    if(intval($components[0]) > intval($currHour)) {
                        $items[] = KGODataObject::parseItem($itemData, $attributeMap);
                        continue;
                    }
                    else if($components[0] == $currHour && intval($components[1]) + 20 >= intval($currMin)) {
                        $items[] = KGODataObject::parseItem($itemData, $attributeMap);
                        continue;
                    }
                }

                else if(substr($time, $timeBreak + 3, 2) == "AM" && date('A') == "PM") {
                    // Need to check if this is close to the current hour
                    if ($currHour == "0" && intval($currMin) <= 20) {
                        $minutes = intval($currMin);
                        if ($components[0] == "12" && intval($components[1] + 20 >= $minutes)) {
                            $items[] = KGODataObject::parseItem($itemData, $attributeMap);
                            continue;
                        }
                    }
                }
            }  
        }
        return $items;
    }

    protected function initializeForPage_bulla(KGOUIPage $page, $objects) {
        $table = new KGOUITable();
        $config = $this->getConfig('page-bulla-config');
        $controller = KGOTableController::factory($config);
        $table->setUIInterfaceDataSource($controller);

        $attributeMap = $this->getConfig('page-bulla-data.map');
        $data = $this->getConfig('page-bulla-data.data');
        $items = $this->selectTimes($data, $attributeMap);

        $controller->setItems($items);
        $page->appendToRegionContents('content', $table);
    }

    protected function initializeForPage_main(KGOUIPage $page, $objects) {
        $table = new KGOUITable();
        $config = $this->getConfig('page-main-config');
        $controller = KGOTableController::factory($config);
        $table->setUIInterfaceDataSource($controller);

        $attributeMap = $this->getConfig('page-main-data.map');
        $data = $this->getConfig('page-main-data.data');
        $items = $this->selectTimes($data, $attributeMap);
        
        $controller->setItems($items);
        $page->appendToRegionContents('content', $table);
    }

    protected function initializeForPage_library(KGOUIPage $page, $objects) {
        $table = new KGOUITable();
        $config = $this->getConfig('page-library-config');
        $controller = KGOTableController::factory($config);
        $table->setUIInterfaceDataSource($controller);

        $attributeMap = $this->getConfig('page-library-data.map');
        $data = $this->getConfig('page-library-data.data');
        $items = $this->selectTimes($data, $attributeMap);

        $controller->setItems($items);
        $page->appendToRegionContents('content', $table);
    }

    protected function initializeForPage_mckenna(KGOUIPage $page, $objects) {
        $table = new KGOUITable();
        $config = $this->getConfig('page-mckenna-config');
        $controller = KGOTableController::factory($config);
        $table->setUIInterfaceDataSource($controller);

        $attributeMap = $this->getConfig('page-mckenna-data.map');
        $data = $this->getConfig('page-mckenna-data.data');
        $items = $this->selectTimes($data, $attributeMap);

        $controller->setItems($items);
        $page->appendToRegionContents('content', $table);
    }

    protected function initializeForPage_compton(KGOUIPage $page, $objects) {
        $table = new KGOUITable();
        $config = $this->getConfig('page-compton-config');
        $controller = KGOTableController::factory($config);
        $table->setUIInterfaceDataSource($controller);

        $attributeMap = $this->getConfig('page-compton-data.map');
        $data = $this->getConfig('page-compton-data.data');
        $items = $this->selectTimes($data, $attributeMap);

        $controller->setItems($items);
        $page->appendToRegionContents('content', $table);
    }

    public function getShuttleWarningTitle() {
        $day = date("N");
        if ($day >= 6) {
            return "Shuttle service is not available today. Here is the weekday schedule";
        }

        return "";
    }

    // protected function initializeForPage_shuttles(KGOUIPage $page, $objects) {
    //     $table = new KGOUITable();
    //     $config = $this->getConfig('page-shuttles-config');
    //     $controller = KGOTableController::factory($config);
    //     $table->setUIInterfaceDataSource($controller);

    //     $attributeMap = $this->getConfig('page-shuttles-data.map');
    //     $data = $this->getConfig('page-shuttles-data.data');
    //     $items = array();
    //     foreach ($data as $itemData) {
    //         $items[] = KGODataObject::parseItem($itemData, $attributeMap);
    //     }

    //     $controller->setItems($items);
    //     $page->appendToRegionContents('content', $table);
    // }
}