class KGOUIFormInputText extends KGOUIFormInput
{
    protected static $dataSourceInterface = 'KGOUIFormInputTextInterface';

    /* Subclassed */
    protected function init() {
        parent::init();

        $this->addField('placeholder', array(
            self::FIELD_FORMATTERS => KGOFormatter::factory('KGOHTMLAttributeFormatter'),
        ));

        $this->addField('autocomplete', array(
            self::FIELD_FORMATTERS => KGOFormatter::factory('KGOHTMLAttributeFormatter'),
            self::JAVASCRIPT_VISIBLE => true,
        ));

        $this->addField('readonly');
    }
}