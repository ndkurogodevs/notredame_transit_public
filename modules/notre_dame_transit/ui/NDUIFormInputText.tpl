{object_wrapper tag=$this->getHTMLWrapperTag()}
    {field_is_set_wrapper field="label" tag="label" for=$this->getInputId() class=$this->getLabelClass()}
    <input type="number" id="{$this->getInputId()}" name="{$this|field:'name'}" value="{$this|field:'value'}"{if $this|field_is_set:'placeholder'} placeholder="{$this|field:'placeholder'}"{/if}{if $this|field:'readonly'} readonly="readonly"{/if} {if $this|field_is_set:'autocomplete'} autocomplete="{$this|field:'autocomplete'}"{/if} />
    {field_is_set_wrapper field="description" class="kgoui_form_input_description"}
{/object_wrapper}
